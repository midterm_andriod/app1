package com.ittipon.myprofile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.util.Log
import android.view.View
import android.widget.TextView
import com.ittipon.myprofile.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    companion object {
        const val TAG = "MainActivity"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.btnHello.setOnClickListener {
            Log.d(TAG, getString(R.string.text_name) + " " + getString(R.string.text_id))
            hello()
        }
    }

    private fun hello() {
        val name: TextView = findViewById(R.id.text_name)
        val intent = Intent(this, HelloActivity::class.java).apply {
            putExtra(EXTRA_MESSAGE, name.text)
        }
        startActivity(intent)
    }


}