package com.ittipon.myprofile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.widget.TextView


class HelloActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)
        val name = intent.getStringExtra(EXTRA_MESSAGE)
        val textViewName = findViewById<TextView>(R.id.text_helloname).apply {
            text = name
        }
    }
}